import React from 'react';
import {Route, Switch} from 'react-router-dom';

//componentes
import App from './Components/App';
import About from './Components/About/index';
import Contact from './Components/Contacts/index';
import Home from './Components/Home/index';
import Ecommerce from './Components/Ecommerce/index';
import Detalles from './Components/Ecommerce/Detalles';
import Page404 from './Components/Page404/index';

const AppRoutes = () =>
<App>
    <Switch>
        <Route exact path='/About' component={About} />
        <Route exact path='/Contacts' component={Contact} />
        <Route exact path='/Ecommerce' component={Ecommerce} />
        <Route exact path='/Detalles' component={Detalles} />
        <Route exact path='/' component={Home} />
        <Route component={Page404} />
    </Switch>
</App>
export default AppRoutes;