import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './Header.css';

import { BrowserRouter, Switch, Route } from 'react-router-dom';
class Header extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired
  }
 constructor(props) {
   super(props);
 }

 render() {
   const {title,items} = this.props;
   return (
    <div className="">
      <h2>{title}</h2>

      <ul className="nav justify-content-center border">
        {
          items && items.map(
            (item,key)=> 
              <li key={key} className="nav-item m-2">
                <Link to={item.url}>{item.title}</Link>
              </li>
            )
        }
      </ul>
   </div>
   );
 }
}

export default Header;
