import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Card.css';

class Card extends Component {
  static propTypes = {
    body: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
  }
  render() {
    const {body} = this.props;
    return(
      <div className="Card">
        {body}
      </div>
    )
  }

}

export default Card;
