import React, { Component } from 'react';

class Detalles extends Component {
    constructor(props){
        super(props);
        this.state={
            hola:[]
        }
    }
    componentDidMount () {
        var url ='http://localhost/xiaomi/api/json_Productos.php'+window.location.search;
        console.log(url);
        fetch(url)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          this.setState({
            hola: data
          });
          console.log(this.state.hola);
        })
        .catch(err => {
          console.log(err);
        })
      }
      
    render() {
        var style ={
            maxwidth: "18 rem",
          }
        var style2 ={
            width: "25%",
            height: "250px"
        }
        var img={
            width: "100%",
            height: "100%"
        }
        return (
            <div>
                {this.state.hola.map(
                    (item,index) =>
                    (
                        <div className="card border-secondary mb-3" style={style}>
                            <div className="card-header text-center"><h3>{item.producto}</h3></div>
                            <div className="card-body text-secondary d-inline-flex p-2 bd-highlight">
                                <div style={style2}>    
                                    <img src={"http://localhost/Xiaomi/imagenes/Miniaturas/"+item.imagen} className="Responsive Image" style={img} alt="..."></img>
                                </div>
                                <div className="ml-5 mt-3">
                                    <h5 className="card-title">Precio</h5>
                                    <p className="card-textd">{item.precio}</p>
                                    <h5 className="card-title">Familia</h5>
                                    <p className="card-text">{item.familia}</p>
                                </div>
                            </div>
                        </div>
                    )   
                )}
            </div>
        );
    }
}

export default Detalles;