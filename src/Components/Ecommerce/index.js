import React, { Component } from 'react';


class Ecommerce extends Component {

 constructor(props) {
   super(props);
   this.state = {
    items: [],
    hideItems: false,
 };
  
}
componentDidMount () {
    fetch('http://localhost/xiaomi/api/json_Productos.php')
    .then(response => response.json())
    .then(data => {
      console.log(data);
      this.setState({
        items: data
      });
    })
    .catch(err => {
      console.log(err);
    })
  }
onClick (event) {
  console.log(event.target.value)
}

 render() {
  var width ={
    width: "15rem"
  }
   return (
   <div className="row px-md-5 mt-3">
    {this.state.items.map(
            (item,index) =>
                (
                 <div className="card my-3 mx-4 border-secondary bg-white rounded " style={width}>
                  <div className="row">
                      <img src={"http://localhost/Xiaomi/imagenes/Miniaturas/"+item.imagen} className="card-img-top p-3" alt="..."></img>
                      <div className="card-body">
                        <h5 className="card-title">{item.producto}</h5>
                        <p className="card-text"> el precio es {item.precio}</p>
                        <button
                        className="btn btn-outline-primary btn-block"
                        value={item.id}
                        onClick={event => this.onClick(event)}>Comprar</button>
                    </div>
                  </div>
                </div>
               )
            )
    }
   </div>
   );
 }
}

export default Ecommerce;
