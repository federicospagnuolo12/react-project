import React, { Component } from 'react'
import '../App.css';
import PropTypes from 'prop-types';

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Header from './Global/Header/Header';
import Footer from './Global/Footer/Footer';
import Card from './Global/Card/Card';
import items from '../data/menu.js';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };
 constructor(props) {
   super(props);
 }
 render() {
  const {children} = this.props;
  
 return (
   <div className="app">
    <div className="container-fluid">
     <Header title="E-Commerce" items={items}></Header>
      <div className="container">
      <div className="row">
        <div className="col mp-10 mx-5 overflow-auto">
          <Card body={children}></Card>
        </div>
      </div>
      </div>


     <Footer></Footer>
    </div>
  </div>
   );
 }
}

export default App;
